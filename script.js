{
//var dataset = [ 25, 7, 5, 26, 11, 8, 25, 14, 23, 19];
var dataset = [];
for (var i = 0; i < 25; i++) {
    var newNumber = Math.random() * 30;
    dataset.push(newNumber);
}

d3.select("#barplot")
  .selectAll("div")
  .data(dataset)
  .enter()
  .append("div")
  .attr("class", "bar")
  .style("height", function(d) {
      return d + "px";
  });

var scatterData = [
    [0, 0],
    [5, 20],
    [480, 90], 
    [250, 50],
    [100, 33]
]

var scatterWidth = 600;
var scatterHeigth = 400;
var padding = 40;

var xScale = d3.scaleLinear()
            .domain([0, d3.max(scatterData, function(d) {
                return d[0];
            }) ])
            .range([padding, scatterWidth - padding]);

var yScale = d3.scaleLinear()
            .domain([0, d3.max(scatterData, function(d) {
                return d[1];
            }) ])
            .range([scatterHeigth - padding, padding]);

var xAxis = d3.axisBottom(xScale)
              .ticks(5);
              //.tickValues([0, 100, 250, 400]);
var yAxis = d3.axisLeft(yScale);

var svg = d3.select("#scatter")
            .append("svg")
            .attr("width", scatterWidth)
            .attr("height", scatterHeigth);

svg.selectAll("circle")
   .data(scatterData)
   .enter()
   .append("circle")
   .attr("cx", function(d) {
        return xScale(d[0]);
   })
   .attr("cy", function(d) {
        return yScale(d[1]);
   })
   .attr("r", 5);

svg.append("g")
   .attr("class", "axis")
   .attr("transform", "translate(0," + (scatterHeigth - padding) + ")")
   .call(xAxis);

   svg.append("g")
   .attr("class", "axis")
   .attr("transform", "translate(" + padding + ",0)")
   .call(yAxis);










}